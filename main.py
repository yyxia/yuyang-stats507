# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd

app = dash.Dash(__name__)
server = app.server

# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options
import numpy as np
import pandas as pd

import plotly.express as px
flights = pd.read_csv("flights.csv.gz")
airports = pd.read_csv("airports.csv")
airlines = pd.read_csv("airlines.csv.gz")
weather = pd.read_csv("weather.csv.gz")

plot1=flights.groupby(['carrier'])['carrier'].count().sort_values(ascending=False)[0:10]
index_list1 = list(plot1.index.values)
list1=plot1.tolist()
df1=pd.DataFrame({'Air Transport Carrier':index_list1,'Total arrivals':list1})
fig1=px.bar(df1,x='Air Transport Carrier',y='Total arrivals')


sr=airports.groupby(['state']).size()
index_list = list(sr.index.values)
list2=sr.tolist()
df2=pd.DataFrame({'state':index_list,'values':list2})
fig2=px.choropleth(df2, locations='state',locationmode='USA-states',color='values',scope='usa',labels={'values':'values'})



app.layout = html.Div(children=[
    html.H1(children='Hello Dash'),

    html.Div(children='''
        Dash: Top 10 flight carriers in this dataset that have the most total arrivals in 2013
    '''),

    dcc.Graph(
        id='example-graph',
        figure=fig1
    ),

    html.Div(children='''
        Dash: Airport count information state-wise
    '''),

    dcc.Graph(
        id='example-graph2',
        figure=fig2
    )
])

if __name__ == '__main__':
    app.run_server(debug=True, port=8080)
